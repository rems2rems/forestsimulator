package controller;

import java.util.ArrayList;
import java.util.List;

import controller.evolution.Evolution;
import controller.evolution.FireSpread;
import controller.evolution.Growth;
import controller.evolution.InfectionSpread;
import model.Forest;
import model.Parcel;
import model.State;

public class Simulation {

	private int nbRows = 100;
	private int nbCols = 100;
	private int tick = 0;
	private boolean auto = true;
	private int nbTicks = 10;
	private int msPerTick = 1000;

	private boolean isPaused;

	private Forest forest;

	private SimulationObserver observer;

	private List<Evolution> evolutions = new ArrayList<>();
	
//	private Evolution growth = new Growth();
//	private Evolution fireSpread = new FireSpread();
//	private Evolution infectionSpread = new InfectionSpread();

	public Simulation(Forest forest) {
		this.forest = forest;
		evolutions.add(new FireSpread());
//		evolutions.add(new InfectionSpread());
//		evolutions.add(new Growth());
		for (int row = 0; row < forest.getHeight(); row++) {
			for (int col = 0; col < forest.getWidth(); col++) {
				forest.getParcel(row, col).setEvolutions(evolutions);
			}
		}
	}

	public int getNbRows() {
		return nbRows;
	}

	public void setNbRows(int nbRows) {
		this.nbRows = nbRows;
	}

	public int getNbCols() {
		return nbCols;
	}

	public void setNbCols(int nbCols) {
		this.nbCols = nbCols;
	}

	public boolean isAuto() {
		return auto;
	}

	public void setAuto(boolean auto) {
		this.auto = auto;
	}

	public int getNbTicks() {
		return nbTicks;
	}

	public void setNbTicks(int nbTicks) {
		this.nbTicks = nbTicks;
	}

	public int getMsPerTick() {
		return msPerTick;
	}

	public void setMsPerTick(int msPerTick) {
		this.msPerTick = msPerTick;
	}

	public void nextTick() {

		State[][] states = new State[forest.getHeight()][forest.getWidth()];
		for (int row = 0; row < forest.getHeight(); row++) {
			for (int col = 0; col < forest.getWidth(); col++) {
				Parcel parcel = forest.getParcel(row, col);
//				List<Parcel> neighbors = forest.getNeighbors(parcel, Neighborhood.MOORE);
//				List<Parcel> orthoNeighbors = forest.getNeighbors(parcel, Neighborhood.VON_NEUMANN);

//				State nextState;
//				try {
//					nextState = fireSpread.nextState(parcel, neighbors);
//				} catch (NonApplicableException e) {
//					try {
//						nextState = infectionSpread.nextState(parcel, orthoNeighbors);
//					} catch (NonApplicableException ee) {
//						nextState = growth.nextState(parcel, neighbors);
//					}
//				}

				states[row][col] = parcel.nextState();
			}
		}
		for (int row = 0; row < states.length; row++) {
			for (int col = 0; col < states[0].length; col++) {
				State state = states[row][col];
				forest.getParcel(row, col).setState(state);
			}
		}

		tick += 1;
	}

	public int getTick() {
		return tick;
	}

	public void setTick(int tick) {
		this.tick = tick;
	}

	public void run() {

//		System.out.println("Tick:" + tick);
//		System.out.println(forest);
//		System.out.println("--------------------------------");
		while (tick < nbTicks && !isPaused) {
			nextTick();

//			System.out.println("Tick:" + tick);
//			System.out.println(forest);
//			System.out.println("--------------------------------");

			if (observer != null) {
				observer.onUpdate(this);
			}

			try {
				Thread.sleep(msPerTick);
			} catch (InterruptedException e) {
			}
		}
//		System.out.println("----  End of simulation !!  -----");
	}

	public void setObserver(SimulationObserver observer) {
		this.observer = observer;
	}

	public boolean isPaused() {
		return isPaused;
	}

	public void pause() {
		this.isPaused = true;
	}

}
