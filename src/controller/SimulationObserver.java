package controller;

public interface SimulationObserver {

	void onUpdate(Simulation simulation);
}
