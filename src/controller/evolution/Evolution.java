package controller.evolution;

import model.Parcel;
import model.State;

public interface Evolution {

	State nextState(Parcel parcel) throws NonApplicableException;
	boolean isApplicable(Parcel parcel);

}