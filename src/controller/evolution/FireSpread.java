package controller.evolution;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;

import model.Neighborhood;
import model.Parcel;
import model.State;

public class FireSpread implements Evolution {
	@Override
	public State nextState(Parcel parcel) throws NonApplicableException {

		
//		List<Parcel> orthoNeighbors = forest.getNeighbors(this, Neighborhood.VON_NEUMANN);

		State state = parcel.getState();
		List<State> neighbors = parcel.getNeighbors(Neighborhood.MOORE).stream().map((p) -> p.getState()).collect(Collectors.toList());
		boolean hasFire = neighbors.stream().anyMatch((s) -> s == State.BURNING);
		boolean isVegetal = state == State.SEED || state == State.SHRUB || state == State.SHRUB_OLD
				|| state == State.TREE;
		float proba = new SecureRandom().nextFloat();

		if (isVegetal && hasFire) {
			if (state == State.SEED && proba < 0.25) {
				return State.BURNING;
			}
			if ((state == State.SHRUB || state == State.SHRUB_OLD) && proba < 0.5) {
				return State.BURNING;
			}
			if (state == State.TREE && proba < 0.75) {
				return State.BURNING;
			}
		}

		switch (state) {
		case BURNING:
			return State.ASHES;
		case ASHES:
			return State.EMPTY;
		default:
			throw new NonApplicableException();

		}
	}

	@Override
	public boolean isApplicable(Parcel parcel) {
		// TODO Auto-generated method stub
		return false;
	}
}
