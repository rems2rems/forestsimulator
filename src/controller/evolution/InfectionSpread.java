package controller.evolution;

import java.security.SecureRandom;
import java.util.List;
import java.util.stream.Collectors;

import model.Neighborhood;
import model.Parcel;
import model.State;

public class InfectionSpread implements Evolution {
	@Override
	public State nextState(Parcel parcel) throws NonApplicableException {

		State state = parcel.getState();
		List<State> neighbors = parcel.getNeighbors(Neighborhood.VON_NEUMANN).stream().map((p) -> p.getState()).collect(Collectors.toList());

		boolean hasInfectedNeighbor = neighbors.stream().anyMatch((s) -> s == State.INFECTED);
		boolean isVegetal = state == State.SEED || state == State.SHRUB || state == State.SHRUB_OLD
				|| state == State.TREE;
		float proba = new SecureRandom().nextFloat();

		if (state == State.INFECTED) {

			return State.EMPTY;
		}
		if (isVegetal && hasInfectedNeighbor) {

			if (state == State.SEED && proba < 1.0) {
				return State.INFECTED;
			}
			if ((state == State.SHRUB || state == State.SHRUB_OLD) && proba < 0.5) {
				return State.INFECTED;
			}
			if (state == State.TREE && proba < 0.5) {
				return State.INFECTED;
			}
		}

		throw new NonApplicableException();
	}
	
	@Override
	public boolean isApplicable(Parcel parcel) throws NonApplicableException {

		State state = parcel.getState();
		List<State> neighbors = parcel.getNeighbors(Neighborhood.VON_NEUMANN).stream().map((p) -> p.getState()).collect(Collectors.toList());

		boolean hasInfectedNeighbor = neighbors.stream().anyMatch((s) -> s == State.INFECTED);
		boolean isVegetal = state == State.SEED || state == State.SHRUB || state == State.SHRUB_OLD
				|| state == State.TREE;

		if (state == State.INFECTED) {

			return true;
		}
		if (isVegetal && hasInfectedNeighbor) {

			return true;
		}

		return false;
	}
}
