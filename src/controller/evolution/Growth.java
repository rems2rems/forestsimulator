package controller.evolution;

import java.util.List;
import java.util.stream.Collectors;

import model.Neighborhood;
import model.Parcel;
import model.State;

public class Growth implements Evolution {
	@Override
	public State nextState(Parcel parcel) {

		State state = parcel.getState();
		List<State> neighbors = parcel.getNeighbors(Neighborhood.MOORE).stream().map((p) -> p.getState()).collect(Collectors.toList());

		int nbBushes = neighbors.stream().filter((s) -> s == State.SHRUB || s == State.SHRUB_OLD)
				.collect(Collectors.toList()).size();
		int nbTrees = neighbors.stream().filter((s) -> s == State.TREE).collect(Collectors.toList()).size();
		boolean isVegetal = state == State.SEED || state == State.SHRUB || state == State.SHRUB_OLD
				|| state == State.TREE;

		if (isVegetal) {
			switch (state) {
			case SEED:
				if ((nbBushes + nbTrees) <= 3) {
					return State.SHRUB;
				}
				return State.SEED;
			case SHRUB:
				return State.SHRUB_OLD;
			default:
				return State.TREE;
			}
		}

		if (nbBushes >= 3 || nbTrees >= 2 || (nbTrees == 1 && nbBushes == 2)) {
			return State.SEED;
		}
		return State.EMPTY;
	}

	@Override
	public boolean isApplicable(Parcel parcel) {
		// TODO Auto-generated method stub
		return false;
	}
}
