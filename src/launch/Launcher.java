package launch;

import view.ForestApp;

public class Launcher {

	public static void main(String[] args) {
		ForestApp.run();
	}
}