package launch;

import java.security.SecureRandom;
import java.util.Random;

import model.Forest;
import model.State;
import model.impl.ForestImpl;

public class ForestBuilder {

	private Forest forest;

	public Forest getForest() {
		return forest;
	}

	private int nbRows, nbCols;

	public ForestBuilder(int nbRows, int nbCols) {
		this.nbRows = nbRows;
		this.nbCols = nbCols;
		forest = new ForestImpl(nbRows, nbCols);
	}

	public ForestBuilder set(int row, int col, State state) {
		forest.getParcel(row, col).setState(state);
		return this;
	}

	public ForestBuilder seed() {
		int nb = (int) (0.2 * nbRows * nbCols);
		seed(nb,State.SEED);
		seed(nb,State.SHRUB);
		seed(nb,State.TREE);
//		seed(nb,State.BURNING);
//		seed(nb,State.INFECTED);
		return this;
	}

	public void seed(int nb,State state) {
		Random rand = new SecureRandom();
		for (int i = 0; i < nb; i++) {
			int row = rand.nextInt(nbRows);
			int col = rand.nextInt(nbCols);
			forest.getParcel(row, col).setState(state);
		}
	}

	public Forest build() {
		return forest;
	}

}
