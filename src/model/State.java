package model;

public enum State {
	EMPTY(" "), SEED("."), SHRUB("o"), SHRUB_OLD("o"), TREE("T"), BURNING("#"), ASHES("_"), INFECTED("@");

	private String symbol;

	private State(String symbol) {
		this.symbol = symbol;
	}

	public String getSymbol() {
		return symbol;
	}
}
