package model;

import java.util.List;

public interface Forest {

	Parcel getParcel(int row, int col);

	public List<Parcel> getNeighbors(Parcel parcel, Neighborhood neighborhood);

	int getWidth();

	int getHeight();
}
