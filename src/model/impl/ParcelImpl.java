package model.impl;

import java.util.List;

import controller.evolution.Evolution;
import controller.evolution.NonApplicableException;
import model.Forest;
import model.Neighborhood;
import model.Parcel;
import model.ParcelObserver;
import model.State;

public class ParcelImpl implements Parcel {

	private State state;
	private int row;
	private int col;
	private Forest forest;
	private List<Evolution> evolutions;
	
	private ParcelObserver observer;

	public ParcelImpl(int row,int col,State state) {
		this.row = row;
		this.setCol(col);
		this.state = state;
	}

	@Override
	public void addObserver(ParcelObserver observer) {
		this.observer = observer;
	}

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
		if (observer != null) {
			observer.onUpdate(this);
		}
	}

	@Override
	public String toString() {
		return state.getSymbol();
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	@Override
	public void setEvolutions(List<Evolution> evolutions) {
		this.evolutions= evolutions;
	}

	@Override
	public State nextState() {
		
		State nextState = state;
		for (Evolution evolution : evolutions) {
			try {
				nextState = evolution.nextState(this);
				break;
			} catch (NonApplicableException e) {
				continue;
			}
		}
		return nextState;
	}

	@Override
	public void setForest(Forest forest) {
		this.forest = forest;
	}

	@Override
	public List<Parcel> getNeighbors(Neighborhood neighborhood) {
		return forest.getNeighbors(this, Neighborhood.MOORE);
	}
}
