package model.impl;

import java.util.ArrayList;
import java.util.List;

import model.Forest;
import model.Neighborhood;
import model.Parcel;
import model.State;

public class ForestImpl implements Forest {

	private Parcel[][] parcels;
	
	public ForestImpl(int nbRows, int nbCols) {
		super();
		parcels = new Parcel[nbRows][nbCols];
		for (int row = 0; row < nbRows; row++) {
			for (int col = 0; col < nbCols; col++) {
				parcels[row][col] = new ParcelImpl(row,col,State.EMPTY);
				parcels[row][col].setForest(this);
			}
		}
	}

	public State getState(int row, int col) {
		return parcels[row][col].getState();
	}

	@Override
	public int getWidth() {
		return parcels[0].length;
	}

	@Override
	public int getHeight() {
		return parcels.length;
	}


	@Override
	public List<Parcel> getNeighbors(Parcel parcel, Neighborhood neighborhood) {
	
		int row = parcel.getRow();
		int col = parcel.getCol();
		
		List<Parcel> neighbors = new ArrayList<Parcel>();

		try {
			neighbors.add(parcels[row - 1][col]);
		} catch (Exception e) {
			/* nothing */}

		try {
			neighbors.add(parcels[row][col - 1]);
		} catch (Exception e) {
			/* nothing */}

		try {
			neighbors.add(parcels[row][col + 1]);
		} catch (Exception e) {
			/* nothing */}

		try {
			neighbors.add(parcels[row + 1][col]);
		} catch (Exception e) {
			/* nothing */}

		if (neighborhood == Neighborhood.MOORE) {
			try {
				neighbors.add(parcels[row - 1][col - 1]);
			} catch (Exception e) {
				/* nothing */}
			try {
				neighbors.add(parcels[row - 1][col + 1]);
			} catch (Exception e) {
				/* nothing */}
			try {
				neighbors.add(parcels[row + 1][col - 1]);
			} catch (Exception e) {
				/* nothing */}
			try {
				neighbors.add(parcels[row + 1][col + 1]);
			} catch (Exception e) {
				/* nothing */}
		}
		return neighbors;
	}

	@Override
	public String toString() {

		String forest = "";
		for (int row = 0; row < parcels.length; row++) {
			for (int col = 0; col < parcels[0].length; col++) {
				forest += parcels[row][col].toString() + " ";
			}
			forest += "|\n";
		}
		return forest;
	}

	@Override
	public Parcel getParcel(int row, int col) {
		return parcels[row][col];
	}
}
