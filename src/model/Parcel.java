package model;

import java.util.List;

import controller.evolution.Evolution;

public interface Parcel {

	void addObserver(ParcelObserver observer);

	State getState();

	void setState(State state);

	public int getRow();

	public void setRow(int row);

	public int getCol();

	public void setCol(int col);
	
	public void setEvolutions(List<Evolution> evolutions);
	
	State nextState();
	
	void setForest(Forest forest);
	
	List<Parcel> getNeighbors(Neighborhood neighborhood);
}