package model;

public interface ParcelObserver {

	void onUpdate(Parcel parcel);

}
