package view;

import java.util.HashMap;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import model.Parcel;
import model.ParcelObserver;
import model.State;

public class ParcelView extends Rectangle implements ParcelObserver {

	Map<State, Color> statesMap = new HashMap<>();
	{
		statesMap.put(State.EMPTY, Color.SADDLEBROWN);
		statesMap.put(State.SEED, Color.LIGHTGREEN);
		statesMap.put(State.SHRUB, Color.GREEN);
		statesMap.put(State.SHRUB_OLD, Color.GREEN);
		statesMap.put(State.TREE, Color.DARKGREEN);
		statesMap.put(State.BURNING, Color.RED);
		statesMap.put(State.ASHES, Color.BLACK);
		statesMap.put(State.INFECTED, Color.PURPLE);

	}

	Parcel parcel;
	private int row;
	private int col;

	public ParcelView(int row, int col, Parcel parcel) {
		this.setRow(row);
		this.setCol(col);
		this.parcel = parcel;
//		state = initState;
		setStroke(Color.BLACK);
		setWidth(15);
		setHeight(15);
		setX(5 * col);
		setY(5 * row);
		updateStateColor();
		setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent arg0) {
				State state = parcel.getState();
				switch (state) {
				case EMPTY:
					state = State.SEED;
					break;
				case SEED:
					state = State.SHRUB;
					break;
				case SHRUB:
				case SHRUB_OLD:
					state = State.TREE;
					break;
				case TREE:
					state = State.INFECTED;
					break;
				case INFECTED:
					state = State.BURNING;
					break;
				case BURNING:
					state = State.ASHES;
					break;
				case ASHES:
					state = State.EMPTY;
					break;
				}
				parcel.setState(state);
				updateStateColor();
			}

		});
	}

	Parcel getParcel() {
		return parcel;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	private void updateStateColor() {
		setFill(statesMap.get(parcel.getState()));
	}

	@Override
	public void onUpdate(Parcel parcel) {
		updateStateColor();
	}

}
