package view;

import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import controller.Simulation;
import controller.SimulationObserver;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import launch.ForestBuilder;
import model.Forest;

public class ForestApp extends Application implements SimulationObserver {

	Forest forest;

	Group g = new Group();
	SimulationObserver observer;
	Label tick;

	Simulation simulation;
	Thread simulThread;

	private static Logger logger = Logger.getLogger("main");

	int nbRows = 100;
	int nbCols = 100;
	
	@Override
	public void start(Stage stage) throws Exception {

		logger.addHandler(new ConsoleHandler());
		logger.setLevel(Level.FINEST);
		for (Handler handler : logger.getHandlers()) {
			handler.setLevel(Level.FINEST);
		}

		forest = new ForestBuilder(nbRows, nbCols).seed().build();

		VBox box = new VBox();
		box.getChildren().add(g);
		for (int row = 0; row < nbRows; row++) {
			for (int col = 0; col < nbCols; col++) {
				ParcelView view = new ParcelView(row, col, forest.getParcel(row, col));
				forest.getParcel(row, col).addObserver(view);
				g.getChildren().add(view);
			}
		}
		Button launch = new Button("launch");
		observer = this;
		launch.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {

				simulation = new Simulation(forest);
				simulation.setNbTicks(1000);
				simulation.setObserver(observer);
				simulThread = new Thread(new Runnable() {
					public void run() {

						simulation.run();
					}
				});
				simulThread.start();
			}

		});
		box.getChildren().add(launch);
		tick = new Label();
		box.getChildren().add(tick);
		Scene scene = new Scene(box, 800, 600);
		stage.setScene(scene);
		stage.show();

	}

	@Override
	public void stop() throws Exception {

		if (simulThread != null && simulThread.isAlive()) {
			simulation.pause();
			simulThread.join();
		}
		super.stop();

	}

	public static void run() {
		launch();
	}

	@Override
	public void onUpdate(Simulation simulation) {
//		logger.info("" + simulation.getTick());
		Platform.runLater(() -> tick.setText("Tick:" + simulation.getTick()));
	}

}